/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Extraction des diff�rent block de l'IP                       *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   extraction_ip.c                                         *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int extraction_ip_block(char ip[], int block);
int extraction_masque(char ip[]);


int extraction_ip_block(char ip[], int block){
    int iterator = 0;
    char ipcp[20];
    strcpy(ipcp, ip);

    // On �clate la chaine grace aux points
    char * chaineEclate = strtok(ipcp, ".");

    //On boucle sur les "block"
    while(chaineEclate != NULL){
        if(iterator == (block-1)){
            return atoi(chaineEclate);
        }
        chaineEclate = strtok(NULL, ".");
        iterator++;
    }

    return 0;
}

int extraction_masque(char ip[]){
    char *chaineEclate;
    int Valeur = 0;
    char ipcp[20];
    strcpy(ipcp, ip);

    // On �clate la chaine grace aux points
    chaineEclate = strtok(ipcp, ".");

    //On boucle sur les "block"
    while(chaineEclate != NULL){
        //Si on a quatre digit Ip on v�rifie le masque
        if(Valeur == 4){
           return atoi(chaineEclate);
        }
        //Passe au masque
        if(Valeur == 2){
           chaineEclate = strtok(NULL, "/");
        }else {
           chaineEclate = strtok(NULL, ".");
        }
        Valeur++;
    }
    return 0;
}

