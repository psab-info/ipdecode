/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Programme de decodage des informations de l'IP               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   decde_ip                                                *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "extraction_ip.h"

char * get_classe_ip(char ip[]);
const char* type_adresse_ip(char ip[]);

//Retourne la classe de l'IP
//Cf : https://fr.wikipedia.org/wiki/Classe_d%27adresse_IP
char * get_classe_ip(char ip[]){
   //Recup�ration du premier block
   int block1 = extraction_ip_block(ip, 1);

   if((block1<=126) & (block1>=0)){
        return "A";
   }else if(block1==127){
        //Ip locale
        return "L";
   }else if((block1>=128) & (block1<=191)){
        return "B";
   }else if((block1>=192) & (block1<=223)){
        return "C";
   }else if((block1>=224) & (block1<=239)){
        //Flux multicast
        return "D";
   }else{
        //Experiences
        return "E";
   }
}

//Retourne le type de l'adresse IP
//CF : RFC 5735
const char* type_adresse_ip(char ip[]){
    //Recup�ration des blocks
    int block1 = extraction_ip_block(ip, 1);
    int block2 = extraction_ip_block(ip, 2);
    int block3 = extraction_ip_block(ip, 3);
    int block4 = extraction_ip_block(ip, 4);

    if(get_classe_ip(ip) == "A"){     //Class A
        if(block1==10){
            //10.0.0.0 � 10.255.255.255
            return "Priv�";
        }else{
            return "Publique";
        }
    }else if(get_classe_ip(ip)=="B"){      //Classe B
        if((block1==172) & (block2>=16) & (block2<=31)){
            //172.16.0.0 � 172.31.255.255
            return "Priv�";
        }else{
            return "Publique";
        }
    }else if(get_classe_ip(ip)=="C"){           //Classe C
        if((block1==192) & (block2==168)){
            //192.168.1.0 � 192.168.255.255
            return "Priv�";
        }else{
            return "Publique";
        }
    }else if(get_classe_ip(ip)=="L"){       //Localhost
        return "LOCAL";
    }else if(get_classe_ip(ip) == "D"){          //Multicast
        return "MULTICAST";
    }else if((block1!=0) & (block2!=0) & (block3!=0) & (block4!=0)){
        return "Publique";
    }

    return "N/A";
}


