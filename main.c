/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Programme Principal                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   Main.c                                                  *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "validation_format.h"
#include "extraction_ip.h"
#include "decode_ip.h"
#include "network_calc.h"
#include "fichier.h"

int main()
{
    char adresse[20] = "";

    printf("Saisissez une IPV4 (saisir . pour arreter la saisie) :\n");   //On demande l'IPV4
    scanf("%s", adresse);               //On lit la chaine tap�

    start_write();      //On efface l'ancien fichier et on �crit le head du nouveau

    while(adresse[0] != '.'){
        if(valide_ipv4(adresse) == 1){
            printf("IP valide. \n");
            printf("Masque = %d \n", extraction_masque(adresse));
            printf("Classe IP = %s \n", get_classe_ip(adresse));
            printf("Type IP = %s \n", type_adresse_ip(adresse));
            printf("Masque en IP = %s \n", cidr_to_maskaddr(adresse));
            printf("L'adresse de sous reseau = %s \n", ip_to_subnetaddr(adresse));
            printf("L'hote est = %s \n", ip_hote(adresse));
            write_to_file(adresse);
        }else {
            printf("IP non valide. \n");
        }

        printf("Saisissez une IPV4 (saisir . pour arreter la saisie) :\n");   //On demande l'IPV4
        scanf("%s", adresse);               //On lit la chaine tap�
    }

    printf("Fin d'execution, les resultats sont enregistre dans result.csv \n");

    return 0;
}


