/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Calculateur sous r�seau et masque                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   network_calc.c                                          *
*                                                                             *
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "extraction_ip.h"

const char* cidr_to_maskaddr(char ip[]);
long decimal_vers_binaire(int digit);
const char* ip_to_subnetaddr(char ip[]);
const char* ip_hote(char ip[]);
int lenHelper(unsigned x);
int binaryToDecimal(long binarynum);


//Transforme du format CIDR en subnet adress ex : /8 en 255.0.0.0 par exemple
const char* cidr_to_maskaddr(char ip[]){
    //On recup�re le mask
    int mask = extraction_masque(ip);
    //On convertis en binaire IPV4 = 32bits
    char bin[32];
    //On ajoute les 1
    for(int i = 0; i < mask; i++){
        strcat(bin, "1");
    }
    //On ajoute les 0 manquants
    for(int i = 0; i < (32 - mask); i++){
        strcat(bin, "0");
    }

    //On reviens en format decimal
    char ipfinal[32];
    //D�coupe block
    char subb1[8];
    char subb2[8];
    char subb3[8];
    char subb4[8];

    memcpy(subb1, &bin[0], 8);
    subb1[8] = '\0';
    _itoa_s(strtol( subb1, NULL, 2),subb1,sizeof(subb1),10);
    strcat(ipfinal, subb1);
    strcat(ipfinal, ".");
    memcpy(subb2, &bin[8], 8 );
    subb2[8] = '\0';
    _itoa_s(strtol( subb2, NULL, 2),subb2,sizeof(subb2),10);
    strcat(ipfinal, subb2);
    strcat(ipfinal, ".");
    memcpy(subb3, &bin[16], 8 );
    subb3[8] = '\0';
    _itoa_s(strtol( subb3, NULL, 2),subb3,sizeof(subb3),10);
    strcat(ipfinal, subb3);
    strcat(ipfinal, ".");
    memcpy(subb4, &bin[24], 8 );
    _itoa_s(strtol( subb4, NULL, 2),subb4,sizeof(subb4),10);
    strcat(ipfinal, subb4);

    return strdup(ipfinal);
}

const char* ip_to_subnetaddr(char ip[]){
    //On recup�re l'IP en 4 block
    char IpBin[32];
    char subb1[9];
    char sub1[9];
    int b = 0;

    //Conversion en binaire des blocks
    for(int i = 0; i < 8; i++){
        subb1[i] = '0';
    }
    sprintf(sub1, "%ld", decimal_vers_binaire(extraction_ip_block(ip, 1)));
    b = 0;
    for(int i = (8 - lenHelper(decimal_vers_binaire(extraction_ip_block(ip, 1)))); i < 8; i++){
        subb1[i] = sub1[b];
        b++;
    }
    for(int i = 0; i < 8; i++){
        IpBin[i] = subb1[i];
    }

    //Block 2
    for(int i = 0; i < 8 ; i++){
        subb1[i] = '0';
    }
    sprintf(sub1, "%ld", decimal_vers_binaire(extraction_ip_block(ip, 2)));
    b = 0;
    for(int i = (8 - lenHelper(decimal_vers_binaire(extraction_ip_block(ip, 2)))); i < 8; i++){
        subb1[i] = sub1[b];
        b++;
    }
    for(int i = 8; i < 16; i++){
        IpBin[i] = subb1[i-8];
    }

    //Block 3
    for(int i = 0; i < 8 ; i++){
        subb1[i] = '0';
    }
    sprintf(sub1, "%ld", decimal_vers_binaire(extraction_ip_block(ip, 3)));
    b = 0;
    for(int i = (8 - lenHelper(decimal_vers_binaire(extraction_ip_block(ip, 3)))); i < 8; i++){
        subb1[i] = sub1[b];
        b++;
    }
    for(int i = 16; i < 24; i++){
        IpBin[i] = subb1[i-16];
    }

    //Block 4
    for(int i = 0; i < 8 ; i++){
        subb1[i] = '0';
    }
    sprintf(sub1, "%ld", decimal_vers_binaire(extraction_ip_block(ip, 4)));
    b = 0;
    for(int i = (8 - lenHelper(decimal_vers_binaire(extraction_ip_block(ip, 4)))); i < 8; i++){
        subb1[i] = sub1[b];
        b++;
    }
    for(int i = 24; i < 32; i++){
        IpBin[i] = subb1[i-24];
    }
    IpBin[32] = '\0';

    //On recup�re le mask
    int mask = extraction_masque(ip);
    //On convertis en binaire IPV4 = 32bits
    char MaskBin[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    //On ajoute les 1
    for(int i = 0; i < mask; i++){
        MaskBin[i] = '1';
    }
    //On ajoute les 0 manquants
    for(int i = mask; (i-mask) < (32 - mask); i++){
        MaskBin[i] = '0';
    }
    MaskBin[32] = '\0';

    //On fait un ET de chaque adresse
    for(int i = 0; i < 32; i++){
        if(IpBin[i] == '1' & MaskBin[i] == '1'){
            IpBin[i] = '1';
        }else{
            IpBin[i] = '0';
        }
    }

    //On retourne vers Decimal
    //Block 1
    char DecBin[32];

    for(int i = 0; i < 4; i++){
        char TmpBin[8];
        for(int f = 0; f < 8; f++){
            TmpBin[f] = IpBin[f+(i*8)];
        }
        char str[10];
        sprintf(str, "%d", binaryToDecimal(strtol(TmpBin, NULL, 10)));
        strcat(DecBin, str);
        if(i != 3){
            strcat(DecBin, ".");
        }
        for(int f = 0; f < 8; f++){
            TmpBin[f] = '0';
        }
    }
    strcat(DecBin, "/");
    char str[10];
    sprintf(str, "%d", extraction_masque(ip));
    strcat(DecBin, str);

    return strdup(DecBin);
}

const char* ip_hote(char ip[]){
    if(ip != ip_to_subnetaddr(ip)){
        return strdup(ip);
    }else{
        return strdup("N/A");
    }
}

//Convertis en binaire
long decimal_vers_binaire(int n) {
    long long bin = 0;
    int rem, i = 1;
    while (n != 0) {
        rem = n % 2;
        n /= 2;
        bin += rem * i;
        i *= 10;
    }
    return bin;
}

//Binaire vers decimal
int binaryToDecimal(long binarynum)
{
    int decimalnum = 0, temp = 0, remainder;
    while (binarynum!=0)
    {
        remainder = binarynum % 10;
        binarynum = binarynum / 10;
        decimalnum = decimalnum + remainder*pow(2,temp);
        temp++;
    }
    return decimalnum;
}

//Retourne la longeur
int lenHelper(unsigned x) {
    if (x >= 1000000000) return 10;
    if (x >= 100000000)  return 9;
    if (x >= 10000000)   return 8;
    if (x >= 1000000)    return 7;
    if (x >= 100000)     return 6;
    if (x >= 10000)      return 5;
    if (x >= 1000)       return 4;
    if (x >= 100)        return 3;
    if (x >= 10)         return 2;
    return 1;
}
