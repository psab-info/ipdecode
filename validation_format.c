/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Validation du format de l'addresse donn�e                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   validation_format.c                                     *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int valide_ipv4(char ip[]);
int est_un_chiffre_valide_ip(char *str);
int est_un_chiffre_valide_masque(char *str);

//Dit si l'ip renseign� est valide
int valide_ipv4(char ip[]){
    char *chaineEclate;
    int Valeur = 0;
    char ipcp[20];
    strcpy(ipcp, ip);

    //On v�rifie si une chaine est bien pr�sente
    if(ipcp == NULL){
        return 0;
    }
    // On �clate la chaine grace aux points
    chaineEclate = strtok(ipcp, ".");

    //On boucle sur les "block"
    while(chaineEclate != NULL){
        //Si on a quatre digit Ip on v�rifie le masque
        if(Valeur == 4 && est_un_chiffre_valide_masque(chaineEclate) == 0){
           return 0;
        }
        //On v�rifie que nous avons bien un chiffre valide pour IP
        if(est_un_chiffre_valide_ip(chaineEclate) == 0 && Valeur != 4){
            return 0;
        }else if(Valeur != 4){
            Valeur++;
        }

        //Passe au masque
        if(Valeur == 3){
           chaineEclate = strtok(NULL, "/");
        }else {
           chaineEclate = strtok(NULL, ".");
        }
    }

    if(Valeur == 4){
        return 1;
    }else{
        return 0;
    }
}

//Est un chiffre compris entre 0 et 255
int est_un_chiffre_valide_ip(char *str){
    int length,i,num;
    length = strlen(str);
    for (i=0;i<length; i++){
        if (!isdigit(str[i]))
        {
            return 0;
        }else{
            num = atoi(str);
            if (num < 0 || num > 255) {
                return 0;
            }
        }
    }
    return 1;
}

//v�rifie si le chiffre est valide pour �tre un masque
int est_un_chiffre_valide_masque(char *str){
    int length,i,num;
    length = strlen(str);
    for (i=0;i<length; i++){
        if (!isdigit(str[i]))
        {
            return 0;
        }else{
            num = atoi(str);
            if (num < 1 || num > 32) {
                return 0;
            }
        }
    }
    return 1;
}
