const char* cidr_to_maskaddr(char ip[]);
long decimal_vers_binaire(int digit);
const char* ip_to_subnetaddr(char ip[]);
const char* ip_hote(char ip[]);
int lenHelper(unsigned x);
int binaryToDecimal(long binarynum);
