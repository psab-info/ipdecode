# Le projet

Ce projet � pour but d'analyser une adresse IP en donnant les informations la constituant.
Les informations sont sauvagarder en CSV dans result.csv

# Les fonctions
 - valide_ipv4
	Entr�e : l'IP
	Sortie : 1 ou 0 si l'IP est valide.
 - extraction_masque
	 Entr�e : l'IP
	 Sortie : le masque en entier si l'ip en comporte un, sinon 0.
 - get_classe_ip
	 Entr�e : l'IP
	 Sortie : la classe de l'IP, un caract�re. (A / L / B / C / D / E)
 - type_adresse_ip
	 Entr�e : l'IP
	 Sortie : Publique / Priv� / LOCAL / MULTICAST (char *)
 - cidr_to_maskaddr
	 Entr�e : l'IP
	 Sortie : Masque sous forme IP (EX : 255.0.0.0)
 - ip_to_subnetaddr
	 Entr�e : l'IP
	 Sortie : Adresse de sous r�seau (EX : 1.0.0.0/8)
 - ip_hote
	 Entr�e : l'IP
	 Sortie : IP de l'h�te (1.0.0.1)

# Autres
Auteur : RICHARD Mathieu (A1 D1)
Projet M2101

