/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  Sujet 2 Analyse Adresse IP                                  *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� :    Gestion �criture dans le fichier de r�sultat                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :   RICHARD-Mathieu                                            *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   fichier.c                                               *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "network_calc.h"


int start_write();
int write_to_file(char ip[]);

int start_write(){

    FILE *fptr;
    remove("result.csv");           //on supprime l'ancien fichier
    fptr = fopen("result.csv","w"); //On ouvre le nouveau

    if(fptr == NULL){
        return 0;
    }
    fprintf(fptr,"%s", "IP,Classe,Type,Masque, SousReseau,Hote \n"); //On �crit le nom des colones

    fclose(fptr); //On ferme le fichier

    return 1;
}


int write_to_file(char adresse[]){
    FILE *fptr;
    //On ouvre le fichier
    fptr = fopen("result.csv","a");
    if(fptr == NULL){
        return 0;
    }
    //On �crit les informations dans le fichier
    fputs("ok", fptr);
    fputs(",", fptr);
    fputs(get_classe_ip(adresse), fptr);
    fputs(",", fptr);
    fputs(type_adresse_ip(adresse), fptr);
    fputs( ",", fptr);
    fputs(cidr_to_maskaddr(adresse), fptr);
    fputs(",", fptr);
    fputs(ip_to_subnetaddr(adresse), fptr);
    fputs(",", fptr);
    fputs(ip_hote(adresse), fptr);
    fputs(" \n", fptr);

    fclose(fptr);
}
